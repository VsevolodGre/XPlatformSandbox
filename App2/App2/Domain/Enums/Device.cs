﻿namespace App2.Domain.Enums
{
    public enum Device
    {
        AndroidEmulatorGeneric,
        AndroidPhysicalGeneric,
        iOSEmulatorGeneric,
        iOSPhysicalGeneric,
    }
}
