﻿using App2.Domain.Requests;
using System.Threading.Tasks;

namespace App2.Domain.Handlers.Requests
{
    interface ICountryFromIPRequestHandler
    {
        Task<CountryFromIPRequestResult> Handle(CountryFromIPRequest request);
    }
}
