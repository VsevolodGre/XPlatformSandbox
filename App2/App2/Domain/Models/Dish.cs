﻿namespace App2.Domain.Models
{
    public class Dish
    {
        public string Name { get; set; }
        public string Price { get; set; }
    }
}
