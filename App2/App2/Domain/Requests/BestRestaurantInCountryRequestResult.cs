﻿using App2.Domain.Enums;
using App2.Domain.Models;

namespace App2.Domain.Requests
{
    public class BestRestaurantInCountryRequestResult
    {
        public GenericOperationStatus Status { get; set; }
        public Restaurant Restaurant { get; set; }
        public string Message { get; set; }
    }
}
