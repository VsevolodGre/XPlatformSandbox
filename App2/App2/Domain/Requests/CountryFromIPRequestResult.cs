﻿using App2.Domain.Enums;

namespace App2.Domain.Requests
{
    public class CountryFromIPRequestResult
    {
        public GenericOperationStatus Status { get; set; }
        public string Country { get; set; }
        public string IP { get; set; }
    }
}
