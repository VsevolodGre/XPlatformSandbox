﻿using App2.Domain.Enums;

namespace App2.Domain.Requests
{
    public class BestRestaurantInCountryRequest
    {
        public string Country { get; set; }
        public Device Device { get; set; }
    }
}
