﻿using App2.Domain.Handlers.Requests;
using App2.Domain.Requests;
using App2.Domain.Enums;
using App2.Services.RestaurantLocators.Kitchry.Models;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;

namespace App2.Services.RestaurantLocators.Kitchry.Handlers.Requests
{
    public class BestRestaurantInCountryRequestHandler : IBestRestaurantInCountryRequestHandler
    {
        public async Task<BestRestaurantInCountryRequestResult> Handle(BestRestaurantInCountryRequest request)
        {
            var bestRestaurantInCountryRequestResult = new BestRestaurantInCountryRequestResult();            

            try
            {
                string endPoint;

                switch (request.Device)
                {
                    case Device.AndroidEmulatorGeneric:
                        endPoint = "http://10.0.2.2";
                        break;

                    default:
                        throw new Exception();
                }

                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(TimeSpan.TicksPerSecond * 3);            
                    var response = await client.GetAsync(endPoint + ":5000/find/" + request.Country);
                    var content = await response.Content.ReadAsStringAsync();

                    if (content.Length != 0)
                    {
                        var restaurant = JsonConvert.DeserializeObject<Restaurant>(content);

                        bestRestaurantInCountryRequestResult.Restaurant = new Domain.Models.Restaurant
                        {
                            Country = restaurant.Country,
                            Name = restaurant.Name,
                            Price = restaurant.Price
                        };

                        bestRestaurantInCountryRequestResult.Message = "We have something for you!";
                    }

                    else
                    {
                        bestRestaurantInCountryRequestResult.Message = "Sorry we were unable to find anything";
                    }                    
                }

                bestRestaurantInCountryRequestResult.Status = GenericOperationStatus.OK;

                return bestRestaurantInCountryRequestResult;
            }

            catch
            {
                bestRestaurantInCountryRequestResult.Status = GenericOperationStatus.Failed;
                bestRestaurantInCountryRequestResult.Message = "Sorry we were to process your request";

                return bestRestaurantInCountryRequestResult;
            }
        }
    }
}
