﻿namespace App2.Services.RestaurantLocators.Kitchry.Models
{
    public class Dish
    {
        public string Name { get; set; }
        public string Price { get; set; }
    }
}
