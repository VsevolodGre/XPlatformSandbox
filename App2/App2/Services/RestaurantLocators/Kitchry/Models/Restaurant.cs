﻿namespace App2.Services.RestaurantLocators.Kitchry.Models
{
    public class Restaurant
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Price { get; set; }
    }
}
