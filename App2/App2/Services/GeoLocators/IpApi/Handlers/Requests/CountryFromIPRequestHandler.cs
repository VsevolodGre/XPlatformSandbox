﻿using App2.Domain.Handlers.Requests;
using App2.Services.GeoLocators.IpApi.Models;
using App2.Domain.Enums;
using App2.Domain.Requests;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;

namespace App2.Services.GeoLocators.IpApi.Handlers.Requests
{
    public class CountryFromIPRequestHandler : ICountryFromIPRequestHandler
    {
        public async Task<CountryFromIPRequestResult> Handle(CountryFromIPRequest request)
        {
            var countryFromIPRequestResult = new CountryFromIPRequestResult();

            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(TimeSpan.TicksPerSecond * 5);

                    var response = await client.GetAsync("http://ip-api.com/json");

                    var content = await response.Content.ReadAsStringAsync();

                    var geolocation = JsonConvert.DeserializeObject<Geolocation>(content);

                    countryFromIPRequestResult.Country = geolocation.Country;
                    countryFromIPRequestResult.IP = geolocation.Query;
                }

                countryFromIPRequestResult.Status = GenericOperationStatus.OK;

                return countryFromIPRequestResult;
            }

            catch
            {
                countryFromIPRequestResult.Status = GenericOperationStatus.Failed;
                countryFromIPRequestResult.Country = "N/A";

                return countryFromIPRequestResult;
            }
        }
    }
}
