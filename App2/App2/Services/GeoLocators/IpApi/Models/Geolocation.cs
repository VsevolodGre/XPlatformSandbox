﻿namespace App2.Services.GeoLocators.IpApi.Models
{
    /// <source>
    /// http://ip-api.com/docs/api:json
    /// </source>
    public class Geolocation
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Query { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string City { get; set; }
    }
}
