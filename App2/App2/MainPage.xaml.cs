﻿using System;
using Xamarin.Forms;
using App2.Domain.Requests;
using App2.Domain.Enums;
using App2.Services.GeoLocators.IpApi.Handlers.Requests;
using App2.Services.RestaurantLocators.Kitchry.Handlers.Requests;

namespace App2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var containerOfProgress = this.FindByName<Label>("labelProgress");
            var containerOfLocation = this.FindByName<Label>("labelLocation");
            var containerOfMessage = this.FindByName<Label>("labelMessage");
            var containerOfRestaurant = this.FindByName<Label>("labelRestaurant");
            var containerOfPrice = this.FindByName<Label>("labelPrice");

            containerOfProgress.Text = "Processing";
            containerOfMessage.Text = "";
            containerOfLocation.Text = "";
            containerOfRestaurant.Text = "";
            containerOfPrice.Text = "";

            var countryFromIPRequest = new CountryFromIPRequest
            {
                IP = "",
            };

            var countryFromIPRequestHandler = new CountryFromIPRequestHandler();

            var countryFromIPRequestResult = await countryFromIPRequestHandler.Handle(countryFromIPRequest);

            containerOfLocation.Text =
                    "You are from " + countryFromIPRequestResult.Country +
                    ". Your IP is " + countryFromIPRequestResult.IP;

            var bestRestaurantInCountryRequest = new BestRestaurantInCountryRequest
            {
                Device = Domain.Enums.Device.AndroidEmulatorGeneric,
                Country = countryFromIPRequestResult.Country,
            };

            var bestRestaurantInCountryRequestHandler = new BestRestaurantInCountryRequestHandler();

            var bestRestaurantInCountryRequestResult = await bestRestaurantInCountryRequestHandler.Handle(bestRestaurantInCountryRequest);

            if (bestRestaurantInCountryRequestResult.Status == GenericOperationStatus.OK)
            {
                containerOfMessage.Text =
                    bestRestaurantInCountryRequestResult.Message;

                containerOfRestaurant.Text = 
                    "Best restaurant is " + bestRestaurantInCountryRequestResult.Restaurant.Name;

                containerOfPrice.Text = 
                    "Average price of a dish there is " + bestRestaurantInCountryRequestResult.Restaurant.Price;
                
                containerOfProgress.Text = "";
            }

            else
            {
                containerOfMessage.Text = "Sorry, we were unable to process your request";

                containerOfProgress.Text = "";
            }
        }        
    }
}
